#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

batches = 10000
# Get a value of N from the terminal
valid = False
while not valid:
    try:
        N = int(input("Enter a value for N: "))
        if N < 1:
            print("N must be greater than 0")
            continue
        valid = True
    except ValueError:
        print("N must be an integer")

# Generate numbers and calculate means
nums = np.random.rand(batches, N)
means = np.mean(nums, axis=1)

# Plot the histogram
plt.tight_layout
plt.rc('font', family='serif')

figure = plt.figure(0)  # histogram
plt.hist(means, bins=100, normed=True, color='#4040ff', lw=0.5)
plt.title('{} means of {} uniform random variables between 0 and 1'
          .format(batches, N))
plt.xlabel('Mean')
plt.ylabel('Bin count')
plt.savefig('{}-{}.pdf'.format(batches, N))
